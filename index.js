import 'ol/ol.css';
import {
  Map,
  proj,
  View
} from 'ol';
import TileLayer from 'ol/layer/Tile';
import XYZ from 'ol/source/XYZ';
import { fromLonLat } from 'ol/proj';

const map = new Map({
  target: 'map',
  layers: [
    new TileLayer({
      source: new XYZ({
        url: 'https://api.mapbox.com/styles/v1/khusak/cjmkdq4hd1ae82sql56e6diyg/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1Ijoia2h1c2FrIiwiYSI6ImNqbWtkYnFiajBwcG8zcGt3YWVobzg0anMifQ.Bpets1t_wrCiGUNfXwj7Yg'
      })
    })
  ],
  view: new View({
    center: fromLonLat([16.84889, 45.89861]),
    zoom: 15
  })
});
